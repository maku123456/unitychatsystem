﻿using Core.Base.Abstract;

namespace Core.Base.Impl
{
    //User Service---------------------------------------------------------------------------
    public class UserData : IUserData
    {
        public string UserStatus { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
        public string PicURL { get; set; }
        public long LastSeen { get; set; }
        /*
        public UserData()
        {
            UserStatus = "This is my status";
            TypeId = 1;
            UserName = "MyName";
            PicURL = "url.jpg";
            LastSeen = 65;
        }*/
    }
}
