﻿using Core.Base.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Base.Impl
{
    public class UserTotalMessageModel : IUserTotalMessage
    {
        /*public UserTotalMessageModel()
        {
            CurrentUserData = new UserData();
            UserMessages = new List<IUserMessage>();
            IUserMessage userMessage = new UserMessage();
            IUserMessage userMessage1 = new UserMessage();
            UserMessages.Add(userMessage);
            UserMessages.Add(userMessage1);
            UserGroups = new List<IGroup>();
            IGroup group = new Group();
            UserGroups.Add(group);
        }*/
        [JsonConverter(typeof(ConcreteConverter<UserData>))]
        public IUserData CurrentUserData { get; set; }
        [JsonConverter(typeof(ConcreteTypeListConverter<IUserMessage, UserMessage>))]
        public List<IUserMessage> UserMessages { get; set; }
        [JsonConverter(typeof(ConcreteTypeListConverter<IGroup, Group>))]
        public List<IGroup> UserGroups { get; set; }
    }
}
