﻿using Core.Base.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;
//Group Srvice----------------------------------------------------------------------

namespace Core.Base.Impl
{
    public class Group : IGroup
    {
        /*
        public Group()
        {
            Admin = new UserUnit();
            GroupMembers = new List<IUserUnit>();
            IUserUnit userUnit = new UserUnit();
            GroupMembers.Add(userUnit);
            UserMessages = new List<IUserMessage>();
            IUserMessage userMessage = new UserMessage();
            UserMessages.Add(userMessage);
            TypeId = 2;
            UserName = "My Name";
            PicURL = "My URL";
        }*/
        [JsonConverter(typeof(ConcreteConverter<UserUnit>))]
        public IUserUnit Admin { get; set; }
        [JsonConverter(typeof(ConcreteTypeListConverter<IUserUnit, UserUnit>))]
        public List<IUserUnit> GroupMembers { get; set; }
        [JsonConverter(typeof(ConcreteTypeListConverter<IUserMessage, UserMessage>))]
        public List<IUserMessage> UserMessages { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
        public string PicURL { get; set; }
    }
}