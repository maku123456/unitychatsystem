﻿using Core.Base.Abstract;
namespace Core.Base.Impl
{
    public class UserUnit : IUserUnit
    {
        /*
        public UserUnit()
        {
            UserStatus = "This is my status";
            TypeId = 1;
            UserName = "MyName";
            PicURL = "Png.url";
            LastSeen = 100;
        }
        */
        public string UserStatus { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
        public string PicURL { get; set; }
        public long LastSeen { get; set; }
    }
}