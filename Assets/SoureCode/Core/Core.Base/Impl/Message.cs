﻿using Core.Base.Abstract;

namespace Core.Base.Impl
{
    public class Message : IMessage
    {
        public Message(bool readStatus, bool messageStatus, long lastSeen, int typeId, string text)
        {
            ReadStatus = readStatus;
            MessageStatus = messageStatus;
            LastSeen = lastSeen;
            TypeId = typeId;
            Text = text;
        }

        /*public Message()
    {
       ReadStatus = false;
       MessageStatus = false;
       LastSeen = 100;
       TypeId = 3;
       Text = "This my message please forward";
    }*/



        public bool ReadStatus { get; set; }
        public bool MessageStatus { get; set; }
        public long LastSeen { get; set; }
        public int TypeId { get; set; }
        public string Text { get; set; }
    }
}
