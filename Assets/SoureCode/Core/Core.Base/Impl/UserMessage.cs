﻿using Core.Base.Abstract;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Base.Impl
{
    public class UserMessage : IUserMessage
    {
        /*
        public UserMessage()
        {
            IMessage message = new Message();
            Messages = new List<IMessage>();
            Messages.Add(message);
            UserStatus = "this is my status";
            TypeId = 1;
            UserName = "My name";
            PicURL = "url";
            LastSeen = 500;
        }*/
        [JsonConverter(typeof(ConcreteTypeListConverter<IMessage, Message>))]
        public List<IMessage> Messages { get; set; }
        public string UserStatus { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
        public string PicURL { get; set; }
        public long LastSeen { get; set; }

        public IMessage GetLatestMessage()
        {
            return null;
        }

        public List<IMessage> GetMessage(int startId, int limit)
        {
            return null;
        }
    }
}
