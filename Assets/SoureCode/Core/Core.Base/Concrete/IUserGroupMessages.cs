﻿using System.Collections.Generic;

namespace Core.Base.Abstract
{
    //-------------------------------Group Unit---------------------------------
    public interface IUserGroupMessages
    {
        List<IGroup> UserGroups { get; set; }
    }
}
