﻿using System.Collections.Generic;

namespace Core.Base.Abstract
{
    //-------------------------------Group Unit---------------------------------
    public interface IGroup : IUnit, IName, IPicUnit
    {
        IUserUnit Admin { get; set; }
        List<IUserUnit> GroupMembers { get; set; }
        List<IUserMessage> UserMessages { get; set; }
    }
}
