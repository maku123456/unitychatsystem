﻿using System;
using System.Collections;
using UnityEngine;

namespace Core.Base.Abstract
{

    //---------------------------- Total unit-------------------------------------
    public interface IUserTotalMessage : IUserMessageList, IUserGroupMessages
    {
        IUserData CurrentUserData { get; set; }
    }
}
