﻿namespace Core.Base.Abstract
{
    //------------------------------------User Unit-----------------------------------

    public interface IUserUnit : IUnit, IName, IPicUnit, IActivity
    {
        string UserStatus { get; set; }
    }


}
