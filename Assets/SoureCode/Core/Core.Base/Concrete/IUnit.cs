﻿namespace Core.Base.Abstract
{
    //------------------------------Generic Unit-------------------------------------
    public interface IUnit
    {
        int TypeId { get; set; }
    }
}
