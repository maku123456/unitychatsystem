﻿using System.Collections.Generic;

namespace Core.Base.Abstract
{
    //-------------------------------USER Message unit--------------------------------------
    public interface IUserMessage : IUserData
    {
        List<IMessage> Messages { get; set; }

        List<IMessage> GetMessage(int startId, int limit);

        IMessage GetLatestMessage();
    }
}
