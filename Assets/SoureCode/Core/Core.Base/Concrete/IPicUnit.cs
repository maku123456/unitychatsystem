﻿namespace Core.Base.Abstract
{
    //------------------------------Generic Unit-------------------------------------

    public interface IPicUnit
    {
        string PicURL { get; set; }
    }
}
