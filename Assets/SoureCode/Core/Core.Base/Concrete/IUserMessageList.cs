﻿using System.Collections.Generic;

namespace Core.Base.Abstract
{
    //-------------------------------USER Message unit--------------------------------------
    public interface IUserMessageList
    {
        List<IUserMessage> UserMessages { get; set; }
    }
}
