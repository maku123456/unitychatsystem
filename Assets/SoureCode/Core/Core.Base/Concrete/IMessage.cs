﻿namespace Core.Base.Abstract
{
    //-----------------------------IMessage Unit----------------------------------------
    public interface IMessage : IActivity, IUnit
    {
        bool ReadStatus { get; set; }//Is it read or not

        bool MessageStatus { get; set; }// this is for message wether stored or deleted

        string Text { get; set; }
    }
}
