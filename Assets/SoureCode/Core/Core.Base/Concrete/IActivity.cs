﻿namespace Core.Base.Abstract
{
    //------------------------------Generic Unit-------------------------------------

    public interface IActivity
    {
        long LastSeen { get; set; }
    }

}
