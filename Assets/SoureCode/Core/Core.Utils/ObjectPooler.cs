﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectPool
{
    GameObject GetObject(bool value);
    void GetBackToPool(GameObject go);
    void ReturnAllToPool();
    List<GameObject> GetAllObjects();
}

public class ObjectPool : IObjectPool
{
    private Transform parent;
    private int poolSize;
    private GameObject prefab;
    private List<GameObject> storage;

    public ObjectPool(GameObject prefab, int poolSize, Transform parent = null)
    {
        this.prefab = prefab;
        this.poolSize = poolSize;
        this.parent = parent;
        Init();
    }

    private void Init()
    {
        storage = new List<GameObject>();

        if (poolSize <= 0) poolSize = 1;

        if (prefab != null)
            CreateObjects();
        else
            Debug.Log("Prefab or Parent should not be null.");
    }

    private void CreateObjects()
    {
        for (int i = 0; i < poolSize; i++)
        {
            AddObject().SetActive(false);
        }
    }

    private GameObject AddObject()
    {
        var go = GameObject.Instantiate(prefab) as GameObject;
        if (parent != null)
            go.transform.SetParent(parent, false);
        storage.Add(go);
        return go;
    }

    public GameObject GetObject(bool value)
    {
        return GetObjectFromPool(value);
    }

    private GameObject GetObjectFromPool(bool value)
    {
        GameObject go = null;

        for (int i = 0; i < storage.Count; i++)
        {
            if (!storage[i].activeSelf)
            {
                go = storage[i];
                break;
            }
        }

        if (go == null)
        {
            var newObject = AddObject();
            newObject.SetActive(value);
            return newObject;
        }

        go.SetActive(value);
        return go;
    }

    public void GetBackToPool(GameObject go)
    {
        if (parent != null)
            go.transform.SetParent(parent, false);
        go.SetActive(false);
    }

    public void ReturnAllToPool()
    {
        foreach (var item in storage)
        {
            item.SetActive(false);
            item.transform.SetParent(parent, false);
        }
    }

    public List<GameObject> GetAllObjects()
    {
        return storage;
    }
}


