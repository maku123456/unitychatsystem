﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

public class ConcreteTypeListConverter<TInterface, TItem> : JsonConverter where TItem : TInterface
{
    public override bool CanConvert(Type objectType)
    {
        //this method is *not* called when using the JsonConverterAttribute to assign a converter
        return IsIList(objectType) || objectType.GetInterfaces().Any(IsIList);
    }

    private static bool IsIList(Type objectType)
    {
        return objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(IList<>);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
        JsonSerializer serializer)
    {
        //explicitly specify the concrete type we want to create
        var specializedList = serializer.Deserialize<List<TItem>>(reader);
        return specializedList.Cast<TInterface>().ToList();
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        //use the default serialization - it works fine
        serializer.Serialize(writer, value);
    }
}
public class ConcreteConverter<T> : JsonConverter
{
    public override bool CanConvert(Type objectType) => true;

    public override object ReadJson(JsonReader reader,
     Type objectType, object existingValue, JsonSerializer serializer)
    {
        return serializer.Deserialize<T>(reader);
    }

    public override void WriteJson(JsonWriter writer,
        object value, JsonSerializer serializer)
    {
        serializer.Serialize(writer, value);
    }
}