using Core.Views.ChatList;
using Core.Views.Group;
using Core.Views.GroupMember;
using Core.Views.PersonalChat;
using Core.Views.Profile;
using UnityEngine;
using Zenject;

public class ViewInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<ChatListView>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ProfileView>().FromComponentInHierarchy().AsSingle();
        Container.Bind<PersonalChatView>().FromComponentInHierarchy().AsSingle();
        Container.Bind<GroupChatView>().FromComponentInHierarchy().AsSingle();
        Container.Bind<GroupMemberView>().FromComponentInHierarchy().AsSingle();
    }
}