﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Core.Base.Abstract;

namespace Core.Views.Group
{
    public interface IGroupListItem
    {
        void SetUp(IGroup group, Action<IGroup> action);
    }

    public class GroupListItem : MonoBehaviour, IGroupListItem
    {
        [SerializeField] private Button btnClick;
        [SerializeField] private TMP_Text txtName;
        [SerializeField] private TMP_Text txtlastMessage;
        protected IGroup groupMessage;
        protected Action<IGroup> action;

        private void OnEnable()
        {
            btnClick.onClick.AddListener(OnClick);
        }
        private void OnDisable()
        {
            btnClick.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            action.Invoke(groupMessage);
        }

        public void SetUp(IGroup groupMessage, Action<IGroup> action)
        {
            this.groupMessage = groupMessage;
            this.action = action;
            txtName.text = groupMessage.UserName;
            txtlastMessage.text = groupMessage.GroupMembers.Count.ToString();
        }
    }
}