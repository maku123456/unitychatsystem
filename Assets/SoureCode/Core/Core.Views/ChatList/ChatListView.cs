﻿using Core.Base.Abstract;
using Core.Base.Impl;
using Core.Views.Group;
using Core.Views.PersonalChat;
using Core.Views.Profile;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


namespace Core.Views.ChatList
{
    public class ChatListView : View<ChatListView>
    {
        [SerializeField] private Settings settings;

        private UserSnapShot userTotalMessage;

        private IObjectPool chatItemPool;
        private IObjectPool groupItemPool;

        [Inject]
        private void Construct(UserSnapShot userTotalMessage)
        {
            this.userTotalMessage = userTotalMessage;
            chatItemPool = new ObjectPool(settings.chatListItem.gameObject, 5, settings.target);
            groupItemPool = new ObjectPool(settings.groupListItem.gameObject, 5, settings.target);
        }

        public override void OnEnable()
        {
            settings.btnProfile.onClick.AddListener(OpenProfileView);
            settings.btnChats.onClick.AddListener(SetChatData);
            settings.btnGroup.onClick.AddListener(SetGroupData);
            SetChatData();
            base.OnEnable();
        }

        public override void OnDisable()
        {
            settings.btnProfile.onClick.RemoveListener(OpenProfileView);
            settings.btnChats.onClick.RemoveListener(SetChatData);
            settings.btnGroup.onClick.RemoveListener(SetGroupData);
            base.OnDisable();
        }

        private void OpenProfileView()
        {
            //Close();
            var view = GoToView<ProfileView>();
            view.InitView(userTotalMessage.UserTotalMessageModel.CurrentUserData);
        }

        private void SetChatData()
        {
            SetActiveChat(true);
            InitChatList();
        }

        private void SetGroupData()
        {
            SetActiveChat(false);
            InitGroupList();
        }

        private void SetActiveChat(bool state)
        {
            settings.btnChats.interactable = !state;
            settings.btnGroup.interactable = state;
            chatItemPool.ReturnAllToPool();
            groupItemPool.ReturnAllToPool();
        }

        private void InitGroupList()
        {
            for (int i = 0; i < userTotalMessage.UserTotalMessageModel.UserGroups.Count; i++)
            {
                var go = groupItemPool.GetObject(false);
                var component = go.GetComponent<IGroupListItem>();
                component.SetUp(userTotalMessage.UserTotalMessageModel.UserGroups[i], OnGroupButtonClick);
                go.SetActive(true);
            }
        }

        private void OnGroupButtonClick(IGroup obj)
        {
            var view = GoToView<GroupChatView>();
            view.InitView(obj);
            Close();
        }

        private void InitChatList()
        {
            for (int i = 0; i < userTotalMessage.UserTotalMessageModel.UserMessages.Count; i++)
            {
                var go = chatItemPool.GetObject(false);
                var component = go.GetComponent<IChatListItem>();
                component.SetUp(userTotalMessage.UserTotalMessageModel.UserMessages[i], OnChatItemClick);
                go.SetActive(true);
            }
        }

        private void OnChatItemClick(IUserMessage obj)
        {
            var view = GoToView<PersonalChatView>();
            view.InitView(obj);
            Close();
        }

        [System.Serializable]
        public class Settings
        {
            public Button btnProfile;
            [Header("Chat Item")]
            public ChatListItem chatListItem;
            public GroupListItem groupListItem;

            public Transform target;
            [Header("Tabs")]
            public Button btnChats;
            public Button btnGroup;
        }
    }
}