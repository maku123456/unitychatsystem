﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Core.Base.Abstract;

namespace Core.Views.ChatList
{ 
public interface IChatListItem
{
    void SetUp(IUserMessage userMessage, Action<IUserMessage> action);
}



    public class ChatListItem : MonoBehaviour, IChatListItem
    {
        [SerializeField] private Button btnClick;
        [SerializeField] private TMP_Text txtName;
        [SerializeField] private TMP_Text txtlastMessage;
        protected IUserMessage userMessage;
        protected Action<IUserMessage> action;

        private void OnEnable()
        {
            btnClick.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            btnClick.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            action.Invoke(userMessage);
        }

        public void SetUp(IUserMessage userMessage, Action<IUserMessage> action)
        {
            this.userMessage = userMessage;
            this.action = action;
            txtName.text = userMessage.UserName;
            txtlastMessage.text = TimeSpan.FromSeconds(userMessage.LastSeen).ToString();
        }

    }
}
