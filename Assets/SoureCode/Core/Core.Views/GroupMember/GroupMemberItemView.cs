﻿using Core.Base.Abstract;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Views.GroupMember
{
    public class GroupMemberItemView : MonoBehaviour
    {
        [SerializeField] private Image pic;
        [SerializeField] private TMP_Text txtName;
        [SerializeField] private TMP_Text txtStatus;
        [SerializeField] private TMP_Text txtLastSeen;

        public void SetUp(IUserUnit userUnit)
        {
            txtName.text = userUnit.UserName;
            txtStatus.text = userUnit.UserStatus;
            txtLastSeen.text = TimeSpan.FromSeconds(userUnit.LastSeen).ToString();
        }
    }
}
