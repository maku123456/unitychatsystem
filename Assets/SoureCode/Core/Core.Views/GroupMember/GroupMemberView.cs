﻿using Core.Base.Abstract;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Core.Views.GroupMember
{
    public class GroupMemberView : View<GroupMemberView>
    {
        [SerializeField] private Button btnBack;
        [SerializeField] private Settings settings;

        private IObjectPool groupMemberPool;

        [Inject]
        private void Construct()
        {
            groupMemberPool = new ObjectPool(settings.groupMemberItem.gameObject, 5, settings.target);
        }
        public override void OnEnable()
        {
            btnBack.onClick.AddListener(OnBackClick);
            base.OnEnable();
        }

        public override void OnDisable()
        {
            btnBack.onClick.RemoveListener(OnBackClick);

            base.OnDisable();
        }

        private void OnBackClick()
        {
            Close();
        }

        public void InitView(IGroup group)
        {
            settings.Name.text = group.UserName;
            settings.members.text = "Memebers: " + group.GroupMembers.Count.ToString();
            InitializeGroupMember(group.GroupMembers);
        }

        private void InitializeGroupMember(List<IUserUnit> users)
        {
            groupMemberPool.ReturnAllToPool();

            for (int i = 0; i < users.Count; i++)
            {
                var go = groupMemberPool.GetObject(false);
                var component = go.GetComponent<GroupMemberItemView>();
                component.SetUp(users[i]);
                go.SetActive(true);
            }
        }

        [System.Serializable]
        public class Settings
        {
            public Image imgProfile;
            public TMP_Text Name;
            public TMP_Text members;

            [Header("Group Memebers")]
            public Transform target;
            public GroupMemberItemView groupMemberItem;
        }
    }
}
