﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Core.Base.Abstract;

namespace Core.Views.Profile
{
    public class ProfileView : View<ProfileView>
    {
        [SerializeField] private Settings settings;

        public override void OnEnable()
        {
            settings.btnClose.onClick.AddListener(OnCloseClick);
            base.OnEnable();
        }

        public override void OnDisable()
        {
            settings.btnClose.onClick.RemoveListener(OnCloseClick);
            base.OnDisable();
        }

        private void OnCloseClick()
        {
            Close();
            // GoToView<ChatListView>();
        }

        public void InitView(IUserData userData)
        {
            settings.userName.text = userData.UserName;
            settings.userStatus.text = userData.UserStatus;
            settings.lastSeen.text = TimeSpan.FromSeconds(userData.LastSeen).ToString();
        }

        [System.Serializable]
        public class Settings
        {
            public Button btnClose;

            public Image imgProfile;
            public TMP_Text userName;
            public TMP_Text userStatus;
            public TMP_Text lastSeen;
        }
    }
}
