﻿using UnityEngine;
using UnityEngine.UI;
namespace Core.Views.PersonalChat
{
    public class ChatItemView : MonoBehaviour
    {
        public Text parentText;
        public Image chatbarImage;
        public Text childText;
        public Image userImage;
    }
}
