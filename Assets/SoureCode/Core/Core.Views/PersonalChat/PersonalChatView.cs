﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Zenject;
using System;
using Core.Base.Abstract;
using Core.Base.Impl;
using Core.Views.Profile;
using Core.Views.ChatList;

namespace Core.Views.PersonalChat
{
    public class PersonalChatView : View<PersonalChatView>
    {
        [SerializeField] private Settings settings;
        [SerializeField] private Button btnBack;
        private IUserMessage userMessage;
        private UserSnapShot UserSnapShot;
        private IObjectPool chatItemPool;
        private string lastUser;

        [Inject]
        private void Construct(UserSnapShot UserSnapShot)
        {
            this.UserSnapShot = UserSnapShot;
            chatItemPool = new ObjectPool(settings.ChatItemView.gameObject, 10, settings.target);
        }
        public override void OnEnable()
        {
            btnBack.onClick.AddListener(OnBackClick);
            settings.btnProfile.onClick.AddListener(OnProfileClick);
            settings.btnSend.onClick.AddListener(OnClickSend);

            base.OnEnable();
        }

        public override void OnDisable()
        {
            btnBack.onClick.RemoveListener(OnBackClick);
            settings.btnProfile.onClick.RemoveListener(OnProfileClick);
            settings.btnSend.onClick.RemoveListener(OnClickSend);

            base.OnDisable();
        }

        public void InitView(IUserMessage userMessage)
        {
            this.userMessage = userMessage;
            settings.txtName.text = userMessage.UserName;
            settings.verticalLayoutGroup = settings.target.GetComponent<VerticalLayoutGroup>();

            ShowUserMsg();
        }

        public void ShowUserMsg()
        {
            chatItemPool.ReturnAllToPool();
            for (int i = 0; i < userMessage.Messages.Count; i++)
            {
                StartCoroutine(ShowUserMsgCoroutine(userMessage.Messages[i]));
            }
        }

        IEnumerator ShowUserMsgCoroutine(IMessage message)
        {
            GameObject go = chatItemPool.GetObject(false);
            var component = go.GetComponent<ChatItemView>();
            go.SetActive(true);
            string msg = message.Text;

            //settings.fontSize = (int)(Screen.height * 0.03f);

            component.parentText.fontSize = settings.fontSize;
            component.childText.fontSize = settings.fontSize;

            component.parentText.text = msg;

            component.childText.color = Color.black;

            yield return new WaitForEndOfFrame();

            float height = go.GetComponent<RectTransform>().rect.height;
            float width = go.GetComponent<RectTransform>().rect.width;

            component.chatbarImage.rectTransform.sizeDelta = new Vector2(width + 5, height + 6);
            component.childText.rectTransform.sizeDelta = new Vector2(width, height);


            component.childText.text = msg;

            if (message.TypeId != UserSnapShot.UserTotalMessageModel.CurrentUserData.TypeId)
            {


                component.userImage.enabled = true;


                component.chatbarImage.color = new Color(settings.userImageColor.r, settings.userImageColor.g, settings.userImageColor.b, 1);
                component.userImage.sprite = settings.userSprite;

                component.chatbarImage.sprite = settings.userChatBarSprite;

                component.userImage.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(-26f, component.userImage.transform.parent.GetComponent<RectTransform>().anchoredPosition.y);
                component.chatbarImage.rectTransform.anchoredPosition = new Vector2(-3f, component.chatbarImage.rectTransform.anchoredPosition.y);

                lastUser = "0";

            }
            else
            {

                if (lastUser == "0")
                {
                    component.userImage.enabled = true;
                }
                else if (lastUser == "1")
                {
                    component.userImage.enabled = false;
                }

                component.chatbarImage.color = new Color(settings.otherImageColor.r, settings.otherImageColor.g, settings.otherImageColor.b, 1);
                component.userImage.sprite = settings.otherUser;

                component.chatbarImage.sprite = settings.otherChatBarSprite;

                component.chatbarImage.rectTransform.anchoredPosition = new Vector2(((settings.target.GetComponent<RectTransform>().rect.width - (settings.verticalLayoutGroup.padding.left + settings.verticalLayoutGroup.padding.right)) - go.GetComponent<RectTransform>().rect.width), component.chatbarImage.rectTransform.anchoredPosition.y);
                component.userImage.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(component.chatbarImage.rectTransform.anchoredPosition.x + component.parentText.rectTransform.rect.width + 27, component.userImage.transform.parent.GetComponent<RectTransform>().anchoredPosition.y);
                lastUser = "1";

            }
            settings.scrollbar.value = 0;

        }

        private void OnProfileClick()
        {
            var view = GoToView<ProfileView>();
            view.InitView((IUserData)userMessage);
        }

        private void OnBackClick()
        {
            Close();
            GoToView<ChatListView>();
        }

        private void OnClickSend()
        {
            var text = settings.ipfTypingText.text;
            if (text == string.Empty)
                return;
            SendMessage(text);
            settings.ipfTypingText.text = string.Empty;
        }
        private void SendMessage(string msg)
        {
            //TODO: add the send logic here
            IMessage msgObj = new Message(false, false, 10, UserSnapShot.UserTotalMessageModel.CurrentUserData.TypeId, msg);
            userMessage.Messages.Add(msgObj);
            StartCoroutine(ShowUserMsgCoroutine(msgObj));
            settings.scrollbar.value = 0;
            UserSnapShot.UpdateShatShot();
            //Upate the Chat snap json
        }



        [System.Serializable]
        public class Settings
        {
            public TMP_Text txtName;
            public Button btnProfile;

            [Header("Chat content")]
            public Transform target;
            public ChatItemView ChatItemView;

            public Sprite userSprite;
            public Sprite otherUser;

            public Color userImageColor;
            public Color otherImageColor;

            public Sprite userChatBarSprite;
            public Sprite otherChatBarSprite;

            public int fontSize;

            public VerticalLayoutGroup verticalLayoutGroup;

            [Header("Send Feature")]
            public Button btnSend;
            public TMP_InputField ipfTypingText;
            public Scrollbar scrollbar;
        }

    }
}
