using Core.Base.Impl;
using Newtonsoft.Json;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ChatSnapShotInstaller", menuName = "Installers/ChatSnapShotInstaller")]
public class ChatSnapShotInstaller : ScriptableObjectInstaller<ChatSnapShotInstaller>
{
    [SerializeField] private TextAsset ChatSnap;
    [Inject] UserSnapShot userSnapShot;
    public override void InstallBindings()
    {
        string json = string.Empty;
        //FIXME: store in the local file as a binary form
        if (!PlayerPrefs.HasKey("CHATS"))
        {
            json = ChatSnap.text;
            PlayerPrefs.SetString("CHATS", json);
        }
        else
        {
            json = PlayerPrefs.GetString("CHATS");
        }
        UserTotalMessageModel userTotalMessageModel = JsonConvert.DeserializeObject<UserTotalMessageModel>(json);
        userSnapShot.SetSnapShot(ChatSnap, userTotalMessageModel);
    }
}