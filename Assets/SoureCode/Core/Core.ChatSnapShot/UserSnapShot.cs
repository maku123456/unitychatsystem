﻿using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;

namespace Core.Base.Impl
{
    public class UserSnapShot
    {
        public void SetSnapShot(TextAsset chatSnap, UserTotalMessageModel userTotalMessageModel)
        {
            UserTotalMessageModel = userTotalMessageModel;
            ChatSnap = chatSnap;
        }

        public void UpdateShatShot()
        {
            var json = JsonConvert.SerializeObject(UserTotalMessageModel);
            PlayerPrefs.SetString("CHATS", json);
        }

        public UserTotalMessageModel UserTotalMessageModel { get; set; }
        public TextAsset ChatSnap { get; set; }
    }
}
