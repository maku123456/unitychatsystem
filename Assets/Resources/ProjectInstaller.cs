using Core.Base.Impl;
using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<ViewController>().AsSingle().NonLazy();
        Container.Bind<UserSnapShot>().AsSingle();
    }
}